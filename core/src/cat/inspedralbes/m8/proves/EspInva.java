package cat.inspedralbes.m8.proves;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class EspInva extends ApplicationAdapter {
	SpriteBatch batch;
	
	//Constants
	final int TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS=1;

	//Model de dades
	Rectangle player;
	List<Rectangle> enemics;
	float tempsUltimEnemic=0; //En aquesta variable acumulem el temps passat des de lultima aparicio d'enemic 

	//Grafics
	Texture playerTexture;
	Texture enemicTexture;

	@Override
	public void create() {
		batch = new SpriteBatch();

		player = new Rectangle(0, 0, 100, 100);
		//Inicialment la llista d'enemics esta buida
		enemics = new ArrayList<Rectangle>();

		playerTexture = new Texture(Gdx.files.internal("naus/player.png"));
		enemicTexture = new Texture(Gdx.files.internal("naus/enemy.png"));
	}


	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// 1. Gestio input
		gestionarInput();

		// 2. Calculs enemics
		actualitza();

		// 3. Dibuixat
		batch.begin();
		batch.draw(playerTexture, player.x, player.y, player.width, player.height);
		//Dibuixem els enemics. Si no n'hi ha cap, no es dibuixa res.
		for (Rectangle enemic : enemics) {
			batch.draw(enemicTexture, enemic.x, enemic.y, enemic.width, enemic.height);
		}

		batch.end();
	}

	private void actualitza() {

		//1. Si ha passat un cert temps apareix un enemic

		// Obtenim el temps que ha passat des de l'ultim dibuixat de pantalla. Es el temps d'un frame.
		float delta = Gdx.graphics.getDeltaTime();

		//L'acumulem al temps per controlar si ha d'apareixer un enemic
		tempsUltimEnemic += delta;
		if(tempsUltimEnemic > TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			int ampladaNauEnemiga = 80;
			int amplePantalla=Gdx.graphics.getWidth() - ampladaNauEnemiga;
			int alturaPantalla=Gdx.graphics.getHeight();
			int x = (int) Math.floor(Math.random()*amplePantalla+1);

//			int x=300;
			//TODO: Genera una y aleatoria
			int y = alturaPantalla;
			
			System.out.println(amplePantalla);
			System.out.println("X: " + x);

			Rectangle nouEnemic=new Rectangle(x, y, ampladaNauEnemiga, 80);
			enemics.add(nouEnemic);

			tempsUltimEnemic=0; //Molt important resetejar el comptador
		}

		//2. Cada enemic baixa un poc. Es fa modificant la seva y
		for (Rectangle enemic : enemics) {
			enemic.y -= 10;
		}

		//Els enemics que surten de la pantalla per baix, son eliminats de la List
		//Eliminar d'una llista mentre s'esta recorrent no es pot fer amb un for normal.
		//Una manera es amb iterators
		for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			Rectangle enemic = (Rectangle) iterator.next();
			//TODO: si es detecta que l'enemic surt de la pantalla, s'esborra de la llista amb l'iterator 
			if (enemic.y <0) {
				iterator.remove();
			}
		}
	}


	private void gestionarInput() {
	
		if (Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A)) {
			player.x -= 15;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D)) {
			player.x += 15;
		}
		
		if (Gdx.input.isKeyPressed(Keys.UP) || Gdx.input.isKeyPressed(Keys.W)) {
			player.y += 15;
		}
		
		if (Gdx.input.isKeyPressed(Keys.DOWN) || Gdx.input.isKeyPressed(Keys.S)) {
			player.y -= 15;
		}

		//Consultem la mida de la pantalla en pixels
		int amplePantalla=Gdx.graphics.getWidth();

		// Consulta l'altura
		int alturaPantalla=Gdx.graphics.getHeight();
		
		//Comprovacio xoc esquerre
		if(player.x <0 ) {
			player.x=0;
		}

		//Comprovacio xoc dret
		if(player.x> (amplePantalla-player.width)) {
			player.x=amplePantalla-player.width;
		}

		// Comprueba el choce por abajo
		if(player.y <0 ) {
			player.y=0;
		}
		
		// Comprueba el choce por arriba
		if(player.y> (alturaPantalla-player.height)) {
			player.y=alturaPantalla-player.height;
		}
//		System.out.println("X: " + player.y);
//		System.out.println("Y: " + player.x);
//		System.out.println(amplePantalla);
	}

	@Override
	public void dispose() {
		batch.dispose();
		playerTexture.dispose();
		enemicTexture.dispose();
	}
}

