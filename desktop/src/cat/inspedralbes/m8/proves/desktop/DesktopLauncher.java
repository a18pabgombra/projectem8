package cat.inspedralbes.m8.proves.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.inspedralbes.m8.proves.EspInva;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "Espace Invaiders";
		config.width = 1200;
		config.height = 600;
		
		new LwjglApplication(new EspInva(), config);
	}
}
